import os
import mmap

import numpy as np
import posix_ipc


def read_bpstats(nbeams, nchans, name='fbfuse_beam_bandpass'):
    """
    Read shared memory segment containing the bandpass statistics, and return
    them as numpy arrays. Raises an exception if the shared memory segment
    with the specified name does not exist.

    Parameters
    ----------
    nbeams: int
        Number of beams in the data
    nchans: int
        Number of channels in the data
    name: str, optional
        Name of the shared memory segment to read.
        (default: 'fbfuse_beam_bandpass')

    Returns
    -------
    bmean: ndarray
        Bandpass mean, as a float32 numpy array with 
        shape=(nbeams, nchans)
    bstd: ndarray
        Bandpass standard deviation, as a float32 numpy array with 
        shape=(nbeams, nchans)
    """
    # The memory layout of the memory segment is:
    # [DADA Header (4096 bytes)][ STATS ]
    # STATS is currently a sequence of float32 tuples (mean, variance), one for
    # each channel index
    head_size = 4096
    stats = ('mean', 'var')
    data_size = nbeams * nchans * np.dtype('float32').itemsize * len(stats)

    shm_size = head_size + data_size
    # NOTE: with the O_RDONLY, the segment needs to exist already
    # otherwise an exception will be raised
    # NOTE 2: The user must have the permission to access this segment,
    # otherwise an exception will be raised. If calling this function from
    # within a container, this will be fine since it has root permissions
    shm = posix_ipc.SharedMemory(name, flags=posix_ipc.O_RDONLY, size=shm_size)

    mm = mmap.mmap(shm.fd, shm.size)
    data = np.frombuffer(mm[head_size:], dtype=np.float32)
    data = data.reshape(nbeams, nchans, len(stats))

    bmean = data[:, :, 0]
    bstd = data[:, :, 1] ** 0.5
    return bmean, bstd
