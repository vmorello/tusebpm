import matplotlib.pyplot as plt


def plot_bpstats(cf, bmean, bstd, beam_id='UNKNOWN'):
    """
    Plot the bandpass statistics for a single beam.

    Parameters
    ----------
    cf: ndarray
        Channel centre frequencies in MHz
    bmean: ndarray
        Bandpass mean, shape=(nbeams, nchans)
    bstd: ndarray
        Bandpass standard deviation, shape=(nbeams, nchans)
    beam_id: str, optional
        ID of the beam to display in the plot title
        (Default: 'UNKNOWN')

    Returns
    -------
    fig: matplotlib.Figure
    """
    if not len(cf) == len(bmean) == len(bstd):
        raise ValueError("Lengths of input arrays do not match")

    fig, axes = plt.subplots(2, 1, sharex=True)
    ax1, ax2 = axes

    ax1.plot(cf, bmean)
    ax1.grid(linestyle=':')
    ax1.set_xlim(cf[0], cf[-1])
    ax1.set_ylabel('Mean')
    ax1.set_title('Beam ID: {}'.format(beam_id))

    ax2.plot(cf, bstd)
    ax2.grid(linestyle=':')
    ax2.set_ylabel('Standard Deviation')
    ax2.set_xlabel('Frequency (MHz)')

    plt.tight_layout()
    return fig
