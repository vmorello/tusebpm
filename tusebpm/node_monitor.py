#
# Katcp client that subscribes to a node's bandpass sensors and records
# the evolving values
#
import signal
import logging

import numpy as np
import tornado
from tornado import gen

from katcp.resource_client import KATCPClientResource


log = logging.getLogger('node_monitor')


class SensorListener(object):
    """ 
    """
    def __init__(self, stat, output):
        self._stat = stat
        self._output = output

    def __call__(self, rt, t, status, value):
        log.debug('Update received for stat = {}, time = {}'.format(self._stat, t))
        # if not len(value):
        #     log.debug('stat value is an empty string, skipping')
        #     return
        data = np.fromstring(value, dtype=np.float16).reshape(6, -1)
        self._output[t] = data
        print(self._output)


class NodeMonitor(KATCPClientResource):
    """ """
    def __init__(self, number, port=5100):
        """ """
        host = "tpn-0-{}".format(number)

        resource_spec = dict(
            name=host,
            address=(host, port),
            controlled=False, # do not expose requests
            auto_reconnect=True,
            auto_reconnect_delay=5.0, # seconds
            )
        super(NodeMonitor, self).__init__(resource_spec)

    def __del__(self):
        self.stop()


def main():
    @gen.coroutine
    def on_shutdown(ioloop):
        log.info("Shutting down")
        ioloop.stop()

    ioloop = tornado.ioloop.IOLoop.current()

    # Hook up to SIGINT so that ctrl-C results in a clean shutdown
    signal.signal(
        signal.SIGINT,
        lambda sig, frame: ioloop.add_callback_from_signal(on_shutdown, ioloop)
        )

    # Dictionaries {timestamp: data}
    output_mean = {}
    output_std = {}

    @gen.coroutine
    def start_and_display():
        monitor = NodeMonitor(65, port=5100)
        yield monitor.start()
        yield monitor.until_synced(timeout=5.0)

        listener_mean = SensorListener('mean', output_mean)
        sensor_mean = monitor.sensor['bandpass_mean']
        sensor_mean.register_listener(listener_mean)
        yield sensor_mean.set_sampling_strategy('event')

        listener_std = SensorListener('std', output_std)
        sensor_std = monitor.sensor['bandpass_std']
        sensor_std.register_listener(listener_std)
        yield sensor_std.set_sampling_strategy('event')

        log.info("Monitor created and set up")

    ioloop.add_callback(start_and_display)
    ioloop.start()


if __name__ == '__main__':
    logging.basicConfig(level='DEBUG')
    logging.getLogger('katcp').setLevel('WARNING')
    main()