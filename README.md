# TUSE Bandpass Monitor

The python side of the TUSE Bandpass Monitor. This module contains functions that read a shared memory buffer containing incoming FBFUSE data, and extract statistics/plots out of it. The buffer gets periodically updated by an external C++ process, as the data flows from FBFUSE.

The module currently contains the following functions:

* `read_bpstats()`: Read the shared memory segment that contains the bandpass statistics written by the `beam_bandpass_shm` executable

* `plot_bpstats()`: Plot bandpass mean and standard deviation for a given beam

* `ccfreqs()`: Generate the list of channel centre frequencies given the centre frequency of the band, bandwidth, and number of channels