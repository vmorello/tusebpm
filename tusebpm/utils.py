import numpy as np

def ccfreqs(fcenter, bw, nchans):
    """
    Returns the list of channel centre frequencies as a numpy array, calculated
    from the given centre frequency of the observed band (fcenter), bandwidth
    (bw), and number of channels (nchans)
    """
    cw = bw / float(nchans) # channel width
    fch1 = fcenter - bw/2.0 + cw/2.0
    return fch1 + cw * np.arange(nchans)
